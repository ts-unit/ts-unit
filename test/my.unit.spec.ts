import * as tsUnit from '../src/tsUnit';

export class MyTest extends tsUnit.TestClass {

    is1() {
        return 1 === 1;
    }  

    is2() {
        return 2 === 2;
    }    
}

var tap = new tsUnit.Test(MyTest).run().getTapResults();    
console.log(tap);
